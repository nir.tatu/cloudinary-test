const { setHeadlessWhen, setCommonPlugins } = require('@codeceptjs/configure');

// turn on headless mode when running with HEADLESS=true environment variable
// export HEADLESS=true && npx codeceptjs run
setHeadlessWhen(process.env.HEADLESS);

// enable all common plugins https://github.com/codeceptjs/configure#setcommonplugins
setCommonPlugins();

require('ts-node/register')

exports.config = {
  tests: './tests/*_test.ts',
  output: './output',
  helpers: {
    Playwright: {
      restart: "session",
      url: "http://localhost",
      show: true,
      browser: 'chromium',
      waitForNavigation: "networkidle0",
      timeout: 120000,
    },
  },
  include: {
    I: './steps_file.ts'
  },
  bootstrap: null,
  mocha: {},
  name: 'cloudinary',

  plugins: {
    allure: {
      enabled: true
    },
    screenshotOnFail: {
      enabled: true,
      uniqueScreenshotNames: true
    },
    autoDelay: {
      enabled: false
    },
    tryTo: {
      enabled: true
    }
  }
}