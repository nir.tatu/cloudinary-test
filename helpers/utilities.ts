const {I} = inject();
const loader = ".react-loading-skeleton";

/**
 * check if loader is not visible
 * @param loaderLocator
 * @param timeoutInSec
 */
export const loaderToBeInvisible = (loaderLocator?: string, timeoutInSec = 20): void => {
    if (typeof loaderLocator === 'undefined') {
        I.waitForInvisible(loader, timeoutInSec);
    } else {
        I.waitForInvisible(loaderLocator, timeoutInSec);
    }
}

/**
 * check (by selector) if element is displayed
 * @param selector
 */
export const isDisplayed = async (selector): Promise<boolean> => {
    return Boolean(await I.executeScript(`let elem = document.querySelector("${selector}");` +
        "$(elem).is(':visible');"));
}


