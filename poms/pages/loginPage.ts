const {I} = inject();

const locators = {
    emailInput: "#user_session_email",
    passwordInput: "#user_session_password",
    signInButton: "#sign-in",
}

export const insertEmail = (email: string) : void => {
 I.fillField(locators.emailInput, email);
}

export const insertPassword = (password: string) : void => {
    I.fillField(locators.passwordInput, password);
}

export const clickOnSignIn = () : void => {
    I.click(locators.signInButton);
}