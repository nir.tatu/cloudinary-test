const {I} = inject();

const locators = {
    welcomeBanner: {
        wrapper: "[class*='GraphicalBannerstyled__Root']",
        title: "[class*='Title']",
    },
}

export const getTitle = async (): Promise<string> => {
    return await I.grabTextFrom(locators.welcomeBanner.title);
}


