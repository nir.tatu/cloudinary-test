const {I} = inject();

export type MENU_OPTIONS = "Manage" | "Edit" | "Analyze" | "Copy URL" | "Download" | "Add to Collection" | "Delete";

export const menuOptions = {
    manage: "Manage",
    edit: "Edit",
    analyze: "Analyze",
    copyURL: "Copy URL",
    download: "Download",
    addToCollection: "Add to Collection",
    delete: "Delete",
};

const locators = {
    uploadButton: "button[data-test='upload-btn']",
    pageTitle: "[data-test='grid-view-toolbar']",
    spinner: "[data-test='grid-loader-spinner']",

    uploadBar: {
        iframe: "[data-test='uw-iframe']",
        wrapper: "[data-test='minimized-upload-queue']",
        done: "[data-test='done-place-holder']",
        complete: "[data-test='complete-place-holder']",
    },

    assets: {
        loader: "[data-test='asset-loader']",
        item: "[data-test='asset-card']",
        name: "[data-test='asset-info-text']",
        badge: "[data-test='asset-badge']",
        menu: {
            wrapper: "[data-test='action-manage-btn']",
            item: "[data-test='item']",
        },
    },

    manage: {
        itemTitle: "span[data-test='item-title']",
    },
}

export const getSpinnerLocator = (): string => {
    return locators.spinner;
}

export const clickOnUploadButton = (): void => {
    I.click(locators.uploadButton);
}

export const getPageTitleLocator = (): string => {
    return locators.pageTitle;
}

export const getUploadButton = (): string => {
    return locators.uploadButton;
}

export const validateUpload = (itemUploaded: number, publicId: string, timeoutInSec: number = 20): void => {
    I.waitForElement(locate(locators.assets.item).withText(publicId), timeoutInSec);
    within({frame: locators.uploadBar.iframe},async () => {
        I.waitForElement(locators.uploadBar.done, timeoutInSec);
        I.see("Done", locators.uploadBar.done);
        I.see(`${itemUploaded} uploaded`, locators.uploadBar.complete);
    });
}

export const rightClickAndChooseFromMenu = (imageName: string, actionFromMenu: MENU_OPTIONS): void => {
    I.rightClick(locate(locators.assets.item).withText(imageName));
    I.click(locate(locators.assets.menu.item).withText(actionFromMenu));
}

export const getTitleManageScreen = async (timeoutInSec: number = 20): Promise<string> => {
    I.waitForElement(locators.manage.itemTitle, timeoutInSec);
    return await I.grabHTMLFrom(locators.manage.itemTitle);
}

