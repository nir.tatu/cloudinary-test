const {I} = inject();
const tryTo = codeceptjs.container.plugins('tryTo');


const locators = {
    wrapper: "[data-test='confirm-dialog']",
    title: "span[data-trigger-id]",
    xButton: "[data-test='modal-close'] button",
    replaceButton: "[data-test='confirm-dialog-confirm-btn']",
    cancelButton: "[data-test='confirm-dialog-cancel-btn']",
}

export const getWrapper = (): string => {
    return locators.wrapper;
}

export const clickOnButton = (buttonName: string): void => {
    switch (buttonName.toLocaleLowerCase()) {
        case "close":
            I.click(locators.xButton);
            break;
        case "replace":
            I.click(locators.replaceButton);
            break;
        case "cancel":
            I.click(locators.cancelButton);
            break;
    }
}

export const isPopupDisplayed = async (timeoutInSec: number = 3): Promise<boolean> => {
    return await tryTo(() => I.waitNumberOfVisibleElements(locators.wrapper,1, timeoutInSec));
}

export const replacePhoto = (timeoutInSec: number = 20): void => {
    I.waitForElement(locators.wrapper, timeoutInSec);
    clickOnButton("replace");
}
