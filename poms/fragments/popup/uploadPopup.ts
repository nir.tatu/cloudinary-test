import {isDisplayed} from "../../../helpers/utilities";
const {I} = inject();


const locators = {
    wrapper: "[data-test='upload-widget-popup']",
    closeButton: "[data-test='close-dialog-icon']",
    advancedButton: "[data-test='btn-advanced']",
    advancedBox: {
        wrapper: "[data-test='advanced-box']",
        publicIdInput: "input[data-test='public-id']",
        tagsInput: "input[data-test='tags-input']",
    },
    browseButton: locate("button").withText("Browse"),
    input: "input[type='file']",
    iframe: "iframe[data-test='uw-iframe']",
}

export const getIframeLocator = (): string => {
    return locators.iframe;
}

export const getWrapper = (): string => {
    return locators.wrapper;
}

export const isPopupDisplayed = async (): Promise<boolean> => {
    return await isDisplayed(locators.wrapper);
}

export const clickOnAdvancedButton = (): void => {
    I.click(locators.advancedButton);
}

export const isAdvancedBoxOpen = async (): Promise<boolean> => {
    return await isDisplayed(locators.advancedBox.wrapper);
}

export const insertPublicId = async (idToInsert: string, openAdvancedBox = true, closeAdvancedBox = true): Promise<void> => {
    if (openAdvancedBox)
        clickOnAdvancedButton();
    I.fillField(locators.advancedBox.publicIdInput, idToInsert);
    if (closeAdvancedBox)
        clickOnAdvancedButton();
}

export const clickOnBrowseButton = (): void => {
    I.click(locators.browseButton);
}

export const uploadPhoto = (pathToPhoto: string): void => {
    I.attachFile(locators.input, pathToPhoto);
}