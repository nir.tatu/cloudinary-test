const {I} = inject();

export type TABS = "Dashboard" | "Media Library" | "Transformations" | "Reports" | "Add ons";

export const tabs = {
    dashboard: "Dashboard",
    mediaLibrary: "Media Library",
    transformations: "Transformations",
    reports: "Reports",
    addOns: "Add ons"
};

const locators = {
    tabs: {
        wrapper: "#header nav",
        dashboard: "[data-balloon='Dashboard'] a",
        mediaLibrary: "[data-balloon='Media Library'] a",
        transformations: "[data-balloon='Transformations'] a",
        reports: "[data-balloon='Reports'] a",
        addOns: "[data-balloon='Add-ons'] a",
    },
    logo: "[data-test='main-bar-logo'] a",
}

export const clickOnTab = (tabName: TABS) : void => {
    switch (tabName) {
        case "Dashboard":
            I.click(locators.tabs.dashboard);
            break;
        case "Media Library":
            I.click(locators.tabs.mediaLibrary);
            break;
        case "Transformations":
            I.click(locators.tabs.transformations);
            break;
        case "Reports":
            I.click(locators.tabs.reports);
            break;
        case "Add ons":
            I.click(locators.tabs.addOns);
            break;
    }
}

export const clickOnLogo = () : void => {
    I.click(locators.logo);
}

