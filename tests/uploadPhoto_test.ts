import 'dotenv/config';
import * as headerBar from "../poms/fragments/headerBar";
import * as mediaLibraryPage from "../poms/pages/mediaLibraryPage";
import * as uploadPopup from "../poms/fragments/popup/uploadPopup";
import * as landingPage from "../poms/pages/landingPage";
import {TABS, tabs} from "../poms/fragments/headerBar";
import * as assert from "assert";
import {getIframeLocator} from "../poms/fragments/popup/uploadPopup";
import {MENU_OPTIONS, menuOptions, rightClickAndChooseFromMenu, validateUpload} from "../poms/pages/mediaLibraryPage";
import * as existingPopup from "../poms/fragments/popup/existingPopup";

const photoToUploadPath = "images/image1.jpeg";
const publicID = "deadpool";
const defaultTimeoutInSec = 20;

Feature('Cloudinary home assignment');

Scenario('Login to cloudinary', async ({I}) => {
    I.login();

    //validating that login completed successfully, and user redirected to landing page
    const title = await landingPage.getTitle();
    assert.strictEqual("Welcome to Cloudinary!", title, `Expected title to be: ${title}`);
});

Scenario('Navigate to Media Library page', async ({I}) => {
    // choose media library tab
    headerBar.clickOnTab(tabs.mediaLibrary as TABS);

    //validating we're on media library page - by checking url and page title
    I.waitForText("Media Library", defaultTimeoutInSec, mediaLibraryPage.getPageTitleLocator());
    const currentURL = await I.grabCurrentUrl();
    assert.strictEqual(currentURL.includes("media_library"), true,
        `Expected current url to include [media_library], current url is: ${currentURL}`);
    I.waitForInvisible(mediaLibraryPage.getSpinnerLocator(), defaultTimeoutInSec);
});

Scenario('Set public id and upload photo', async ({I}) => {
    mediaLibraryPage.clickOnUploadButton();
    //waiting for upload popup to be shown
    I.waitForElement(uploadPopup.getIframeLocator(), defaultTimeoutInSec);

    //changing focus to upload popup iframe, and uploading photo
    within({frame: uploadPopup.getIframeLocator()} , async () => {
        I.waitForElement(uploadPopup.getWrapper(), defaultTimeoutInSec);
        await uploadPopup.insertPublicId(publicID);
        uploadPopup.uploadPhoto(photoToUploadPath);
    });

    //in case we are trying to save photo with public id which exists, we'll replace existing photo
    if (await existingPopup.isPopupDisplayed())
       existingPopup.replacePhoto();

    //validating photo was uploaded successfully - checking upload bar
    validateUpload(1, publicID);
});

Scenario('Verify public id', async ({I}) => {
    //right click on photo and choose "manage"
    mediaLibraryPage.rightClickAndChooseFromMenu(publicID, menuOptions.manage as MENU_OPTIONS);

    //grabbing photo title (aka - public id), and validating that the title is as expected
    const itemTitle = await mediaLibraryPage.getTitleManageScreen();
    assert.strictEqual(itemTitle, publicID, `Expected [${publicID}], but got [${itemTitle}]`);
});
