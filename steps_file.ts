import * as loginPage from "./poms/pages/loginPage";
import {loaderToBeInvisible} from "./helpers/utilities";

module.exports = function() {
  return actor({
    login(): void {
      this.amOnPage(process.env.URL);
      loginPage.insertEmail(`${process.env.USERNAME}`);
      loginPage.insertPassword(`${process.env.PASSWORD}`);
      loginPage.clickOnSignIn();
      this.waitForNavigation();
      loaderToBeInvisible();
    }
  });
}
